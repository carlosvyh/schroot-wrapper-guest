#!/bin/bash


export PATH=$HOME/bin:$PATH
export MAKEFLAGS=-j1
#export SHLVL=1
export LANG=es_PE.UTF-8
export LANGUAGE=es_PE.UTF-8
export LC_ALL=es_PE.UTF-8
export DISPLAY=:0.0
export QT_PREFIX_DIR=/
export QT_BIN_DIR=/

source $HOME/CUSTOM_ENVIRONMENT.sh